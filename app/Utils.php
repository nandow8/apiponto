<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Utils extends Model
{
    static function exibeNomeDoMes($numeroMes){
        $meses = array(
            1 => 'Janeiro',
            2 => 'Fevereiro',
            3 => 'Março',
            4 => 'Abril',
            5 => 'Maio',
            6 => 'Junho',
            7 => 'Julho',
            8 => 'Agosto',
            9 => 'Setembro',
            10 => 'Outubro',
            11 => 'Novembro',
            12 => 'Dezembro'
        );
        $numeroMes = strtolower(substr($numeroMes, 0, 3));
        return $meses[$numeroMes];
    }
    function contadiasdasemana($dia_semana, $mes, $ano) { // exemplo de uso: contadiasdasemana("4","05","2020");
        $Date = new DateTime();
        $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
          for ($dia = 0; $dia <= $dias; $dia++) {
          $Date->setDate( $ano, $mes, $dia );
             if ($Date->format( "w" ) == $dia_semana) {
                if ($dia != 0)
                    $datas[] = $dia."/".$mes."/".$ano;
             }
          }
        return count($datas);
    }

    public static function diaSemanaString() {
        $dias = array(
            1 => 'Segunda-feira',
            2 => 'Terça-feira',
            3 => 'Quarta-feira',
            4 => 'Quinta-feira',
            5 => 'Sexta-feira',
            6 => 'Sabado',
            7 => 'Domingo',
        );
        $numeroMes = date('N');
        return date('d/m/Y') . ' '. $dias[$numeroMes];
    }
}
