<?php

namespace App\Models\Comum;
use App\Models\BaseModel;

class RegistroPonto extends BaseModel
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'registro_pontos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    
}