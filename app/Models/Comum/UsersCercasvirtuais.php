<?php

namespace App\Models\Comum;
use App\Models\BaseModel;

class UsersCercasvirtuais extends BaseModel
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_cercasvirtuais';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    
}