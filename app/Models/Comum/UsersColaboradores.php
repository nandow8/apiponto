<?php

namespace App\Models\Comum;
use App\Models\BaseModel;

class UsersColaboradores extends BaseModel
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_colaboradores';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    
}