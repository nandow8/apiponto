<?php

namespace App\Models\Comum;
use App\Models\BaseModel;

class UsersEscala extends BaseModel
{    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_escalas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function escalas()
    {
        return $this->belongsTo('App\Models\Comum\Escala', 'escalas_id', 'id')
            ->where('empresas_id', auth('api')->user()->empresas_id);
    }
    
}