<?php

namespace App\Models\Admin;
use App\Models\BaseModel;

class Log extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'logs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function users()
    {
        return $this->hasOne('App\User', 'users_id', 'id');
    }
    
}