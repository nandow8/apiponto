<?php

namespace App\Http\Controllers\Colaborador;
use App\Http\Controllers\Controller;
use App\Models\Comum\UsersCercasvirtuais;
use Illuminate\Http\Request;

class UsersCercasvirtuaisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function verificaDistanciaBaterPonto(Request $request) { // verifica se usuario pode acessar recursos no aplicativo
        $coordenadas = UsersCercasvirtuais::select(
                'cercas_virtuais.descricao',
                'cercas_virtuais.lat',
                'cercas_virtuais.lng'
            )
            ->join('cercas_virtuais', 'cercas_virtuais.idunico', 'users_cercasvirtuais.idunico')
            ->where('users_cercasvirtuais.users_id', $request->users_id)
            ->where('users_cercasvirtuais.status', 1)
            ->first();
        
        if (!$coordenadas) // se nao encontra coordenada, é porque nao foi adicionado cerca ao colaborador - entao pode :)
            return response()->json(['status' => 1], 200);

        $distancia = $this->calculaDistancia($coordenadas->lat, $coordenadas->lng, $request->lat, $request->lng);

        if ($distancia <= 0.500) {
            return response()->json(['status' => 1], 200);    
        }

        return response()->json(['status' => 0], 400);    
    }

    function calculaDistancia($lat_inicial, $long_inicial, $lat_final, $long_final){
        $d2r = 0.017453292519943295769236;

        $dlong = ($long_final - $long_inicial) * $d2r;
        $dlat = ($lat_final - $lat_inicial) * $d2r;

        $temp_sin = sin($dlat/2.0);
        $temp_cos = cos($lat_inicial * $d2r);
        $temp_sin2 = sin($dlong/2.0);

        $a = ($temp_sin * $temp_sin) + ($temp_cos * $temp_cos) * ($temp_sin2 * $temp_sin2);
        $c = 2.0 * atan2(sqrt($a), sqrt(1.0 - $a));

        return 6368.1 * $c;
    }
}
