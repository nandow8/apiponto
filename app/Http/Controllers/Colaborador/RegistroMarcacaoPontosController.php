<?php

namespace App\Http\Controllers\Colaborador;
use App\Http\Controllers\Controller;
use App\Models\Admin\Log;
use App\Models\Comum\RegistroPonto;
use App\Models\Comum\UsersColaboradores;
use App\Models\Comum\UsersEscala;
use Illuminate\Http\Request;

use App\Models\RegistroMarcacaoPontos;
use App\Utils;
use DateTime;

class RegistroMarcacaoPontosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show($id) {

        $usersColaboradores = UsersColaboradores::select(
            // 'dispositivos_autorizados_app',
            // 'permite_marcacao_ponto_app',
            // 'consultas_ponto_vizualizar_ultimas_horas_qtd_dias',
            // 'consultas_ponto_vizualizar_ultimas_horas',
            'consultas_ponto_dias',
            'consultas_ponto_select_visualizar_editar',
            'consultas_ponto',
        )
        ->where('users_id', $id)
        ->where('acesso_ao_sistema', 1)
        ->where('permite_ponto_aplicativo', 1)
        ->where('status', 1)
        ->first(); 

        if (!$usersColaboradores)
            return response()->json(['status' => 'Colaborador inexistente ou desabilitado do sitema'], 400);    
        
        $quantidadeDiasColaboradorVisualiza = date('Y-m-d', strtotime("-$usersColaboradores->consultas_ponto_dias days",strtotime(date('Y-m-d')))); 

        $registros = RegistroMarcacaoPontos::select(
            'id',
            'registro_pontos_id',
            'dia_semana_string',
            'aprovado_reprovado',
            'permissao',
            'hora_ponto',
            'created_at',
            'foto',
            'origem_ponto',
            'justificativa_colaborador',
            'observacao_colaborador',
            'justificativa_empresa',
            'observacao_empresa'
        )
        ->where('users_id', $id)
        // ->whereMonth('created_at', date('m'))
        // ->whereYear('created_at', date('Y'))
        //->whereBetween('created_at', [$quantidadeDiasColaboradorVisualiza, date('Y-m-d')])
        ->whereDate('created_at', '>=', $quantidadeDiasColaboradorVisualiza)
        ->where('permissao', '!=', 4)
        ->orderBy('created_at', 'ASC')
        ->get();
        
        $array = [];
        $collection = collect($registros)->groupBy('dia_semana_string');
        foreach ($collection as $key => $value) {
             $array[$key] = $value;
        }
        
        $reverso = null;
        
        krsort($array);
        foreach ($array as $chave => $valor) {
            $reverso[$chave] = $valor;
        }

        return response()->json(
            [
                'permissoes' => $usersColaboradores,
                'marcacoes_de_pontos' => $reverso,
            ],
        );
    }

    public function store(Request $request)
    {
        $data = $request->all();
        
        if (false == $this->verificaSePodeBaterPonto($data['users_id'], $data['empresas_id'])) {
            return ['message' => 'Não foi possível bater o ponto, por favor, verifique a sua escala.'];
        }

        $data['registro_pontos_id'] = $this->verificaPontoHoje($data['users_id'], $data['empresas_id'])['id'];

        if($request->foto){
            //  $img_name = $data['users_id'] . $data['empresas_id'] .date("HisdmY");
            //  $file_name = $request->file('foto')->move('img/colaboradores/pontos', "$img_name.jpeg");

            $caminho = env('APP_ENV') !== 'local' ? 'public/' : '';

            $image_base64 = base64_decode($data['foto']);
            $file_name = $caminho . 'img/colaboradores/pontos' . DIRECTORY_SEPARATOR . uniqid() . '.jpeg';
            file_put_contents($file_name, $image_base64);
        } else {
            $file_name = null;
        }
        
        $registroponto = RegistroMarcacaoPontos::create([
            'empresas_id' => $data['empresas_id'],
            'users_id' => $data['users_id'],
            'registro_pontos_id' => $data['registro_pontos_id'],
            'foto' => $file_name,
            'observacao_colaborador' => $data['observacao_colaborador'],
            'origem_ponto' => 'APLICATIVO',
            'permissao' => 1,
            'dia_semana_string' => Utils::diaSemanaString(),
            'hora_ponto' => str_pad(date('G:i'), 5, '0',  STR_PAD_LEFT),
            'numero_recibo' => $data['empresas_id'] . $data['users_id'] . date('d') . date('Y') . date('m') . date('s') . date('h') . date('i')
        ]);

        Log::create([
            'empresas_id' => $data['empresas_id'],
            'users_id' => $data['users_id'],
            'tabela' => RegistroMarcacaoPontos::class,
            'tabela_id' => $registroponto->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($registroponto, 200);
    }

    public function verificaSePodeBaterPonto($users_id, $empresas_id) {
        $diaAtual = date('N');

        $escala = UsersEscala::select('escalas.dia_jornada')
            ->join('escalas', 'escalas.id', 'users_escalas.escalas_id')
            ->where('users_escalas.users_id', $users_id)
            ->where('users_escalas.empresas_id', $empresas_id)
            ->where('users_escalas.status', 1)
            ->first();

        if ($escala->dia_jornada < 8 AND $escala->dia_jornada == $diaAtual) //unico dia da semana
            return true;
        
        if ($escala->dia_jornada == 8 AND $diaAtual > 5) // somente sabado E domingo
            return true;
        
        if ($escala->dia_jornada == 9) {
            $data1 = new DateTime();
            $data2 = $this->buscaDia($users_id, $empresas_id);
            
            $dataDIA = new DateTime($data2);
            if ($dataDIA->format('d') == date('d'))
                return true;

            if (
                $data2->diff($data1)->format('%d') == 1
                AND $data2->diff($data1)->format('%h') == 11
	            AND $data2->diff($data1)->format('%i') >= 50)
                    return true;

            return false;
                //Faltam %Y Anos %m Mês, %d dias e %h horas %i minutos
        }

        if ($escala->dia_jornada == 10) {
            $data1 = new DateTime();
            $data2 = $this->buscaDia($users_id, $empresas_id);
            
            $dataDIA = new DateTime($data2);
            if ($dataDIA->format('d') == date('d'))
                return true;

            if (
                $data2->diff($data1)->format('%d') == 2
                AND $data2->diff($data1)->format('%h') == 23
                AND $data2->diff($data1)->format('%i') >= 50)
                    return true;
            
            return false;
        }

        if ($escala->dia_jornada == 11) // livre
            return true;

        if ($escala->dia_jornada == 12 AND $diaAtual < 6) // segunda a sexta
            return true;    

            
        return false;
    }

    public function verificaPontoHoje($users_id, $empresas_id) {
        $verificaPontoHoje = RegistroPonto::where('empresas_id',  $empresas_id)
            ->where('users_id',  $users_id)
            ->where('status',  1)
            ->whereYear('created_at', date('Y'))
            ->whereMonth('created_at', date('m'))
            ->whereDay('created_at', date('d'))
            ->count();

        if ($verificaPontoHoje > 0) {
            return RegistroPonto::select('id')
                ->where('empresas_id',  $empresas_id)
                ->where('users_id',  $users_id)
                ->where('status',  1)
                ->whereYear('created_at', date('Y'))
                ->whereMonth('created_at', date('m'))
                ->whereDay('created_at', date('d'))
                ->first();
        } else {
            return RegistroPonto::create([
                'empresas_id' => $empresas_id,
                'users_id' => $users_id,
            ]);
        }
    }

    public function buscaDia($users_id, $empresas_id) {
        return RegistroMarcacaoPontos::select('created_at')
                ->where('users_id', $users_id)
                ->where('empresas_id', $empresas_id)
                ->orderBy('updated_at', 'DESC')
                ->first();
    }

    public function primeiraVerificaSePodeBaterPonto(Request $request) {
        // $verificaSePode = UsersColaboradores::select(
        //     'users_id',
        //     'acesso_ao_sistema',
        //     'status',
        //     'permite_ponto_aplicativo'
        // )
        // ->where('users_id', $request->users_id)
        // ->where('empresas_id', $request->empresas_id)
        // ->where('acesso_ao_sistema', 1)
        // ->where('permite_ponto_aplicativo', 1)
        // ->where('status', 1)
        // ->first();   
        
        // if (!$verificaSePode)
        //     return response()->json(['status' => 'Não tem acesso ao sistema.'], 400);
        
        $diaAtual = date('N');

        $escala = UsersEscala::select('escalas.dia_jornada')
            ->join('escalas', 'escalas.id', 'users_escalas.escalas_id')
            ->join('users_colaboradores', 'users_colaboradores.users_id', 'users_escalas.users_id')
            ->where('users_escalas.users_id', $request->users_id)
            ->where('users_escalas.empresas_id', $request->empresas_id)
            ->where('users_escalas.status', 1)
            ->where('users_colaboradores.acesso_ao_sistema', 1)
            ->where('users_colaboradores.permite_ponto_aplicativo', 1)
            ->where('users_colaboradores.status', 1)
            ->first();

        if (!$escala)
            return response()->json(['status' => 'Colaborador não encontrado'], 500);

        if ($escala->dia_jornada < 8 AND $escala->dia_jornada == $diaAtual) //unico dia da semana
            return response()->json(['status' => 'OK'], 200);
        
        if ($escala->dia_jornada == 8 AND $diaAtual > 5) // somente sabado E domingo
            return response()->json(['status' => 'OK'], 200);
        
        if ($escala->dia_jornada == 9) {
            $data1 = new DateTime();
            $data2 = $this->buscaDia($request->users_id, $request->empresas_id);
            
            $dataDIA = new DateTime($data2);
            if ($dataDIA->format('d') == date('d'))
                return response()->json(['status' => 'OK'], 200);

            if (
                $data2->diff($data1)->format('%d') == 1
                AND $data2->diff($data1)->format('%h') == 11
	            AND $data2->diff($data1)->format('%i') >= 50)
                    return response()->json(['status' => 'OK'], 200);

            return response()->json(['status' => 'Não foi possível registrar o ponto, por favor, verifique a sua escala.'], 400);
                //Faltam %Y Anos %m Mês, %d dias e %h horas %i minutos
        }

        if ($escala->dia_jornada == 10) {
            $data1 = new DateTime();
            $data2 = $this->buscaDia($request->users_id, $request->empresas_id);
            
            $dataDIA = new DateTime($data2);
            if ($dataDIA->format('d') == date('d'))
                return response()->json(['status' => 'OK'], 200);

            if (
                $data2->diff($data1)->format('%d') == 2
                AND $data2->diff($data1)->format('%h') == 23
                AND $data2->diff($data1)->format('%i') >= 50)
                    return response()->json(['status' => 'OK'], 200);
            
            return response()->json(['status' => 'Não foi possível registrar o ponto, por favor, verifique a sua escala.'], 400);
        }

        if ($escala->dia_jornada == 11) // livre
            return response()->json(['status' => 'OK'], 200);

        if ($escala->dia_jornada == 12 AND $diaAtual < 6) // segunda a sexta
            return response()->json(['status' => 'OK'], 200);    

            
        return response()->json(['status' => 'Não foi possível registrar o ponto, por favor, verifique a sua escala.'], 400);
    }

    public function updateBaterPonto(Request $request) {
        
        $registroponto = RegistroMarcacaoPontos::findOrFail($request->id);
        $timestamp = strtotime($registroponto->created_at);

        $registroponto->update([
            'created_at' => strtotime(date('Y-m-d', $timestamp) . ' ' .  str_pad($request->hora_ponto, 5, '0',  STR_PAD_LEFT)),
            'justificativa_colaborador' => $request->justificativa_colaborador,
            'observacao_colaborador' => $request->observacao_colaborador,
            'hora_ponto' => str_pad($request->hora_ponto, 5, '0',  STR_PAD_LEFT),
            'permissao' => 0
        ]);

        Log::create([
            'empresas_id' => $request->empresas_id,
            'users_id' => $request->users_id,
            'tabela' => RegistroMarcacaoPontos::class,
            'tabela_id' => $request->id,
            'atividade' => 'Solicitou ajuste de ponto'
        ]);

        return response()->json($registroponto, 200);
    } 

    public function subirImagemColaborador(Request $request)
    {
            //  $img_name = $data['users_id'] . $data['empresas_id'] .date("HisdmY");
            //  $file_name = $request->file('foto')->move('img/colaboradores/pontos', "$img_name.jpeg");

            $caminho = env('APP_ENV') !== 'local' ? 'public/' : '';

            $image_base64 = base64_decode($request->foto);
            $file_name = $caminho . $request->fileName;
            file_put_contents($file_name, $image_base64);
            return true;
    }
}
