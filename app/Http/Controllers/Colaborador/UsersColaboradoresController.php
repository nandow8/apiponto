<?php

namespace App\Http\Controllers\Colaborador;
use App\Http\Controllers\Controller;
use App\Models\Comum\UsersCercasvirtuais;
use App\Models\Comum\UsersColaboradores;

class UsersColaboradoresController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function verificaPermissoes($id) { // verifica se usuario pode acessar recursos no aplicativo
        $user = UsersColaboradores::select(
                'dispositivos_autorizados_app',
                'permite_marcacao_ponto_app',
                'consultas_ponto_vizualizar_ultimas_horas_qtd_dias',
                'consultas_ponto_vizualizar_ultimas_horas',
                'consultas_ponto_dias',
                'consultas_ponto_select_visualizar_editar',
                'consultas_ponto',
                'acesso_ao_sistema',
                'permite_ponto_aplicativo'
            )
            ->where('users_id', $id)
            ->where('acesso_ao_sistema', 1)
            ->where('permite_ponto_aplicativo', 1)
            ->where('status', 1)
            ->first();

        if ($user) {
            return response()->json(
                [
                    'user_colaboradores' => $user,
                ],
            );
        }

        return response()->json(['status' => 0], 400);    
    }
}
