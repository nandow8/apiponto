<?php
namespace App\Http\Controllers\authenticate;

use App\Http\Controllers\Controller;
use App\Models\Comum\UsersCercasvirtuais;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Users;
use App\User;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    public function __construct()
    {
        //  $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
            'codigo' => 'required',
        ]);

        $colaborador = $this->buscaColaborador($request->input('email'), $request->input('codigo'));
        
        if (!$colaborador)
            return response()->json(['status' => 'Dados inválidos'], 401);

        if (Hash::check($request->input('password'), $colaborador['user']['password'])) {
            $token = base64_encode(Str::random(40));
            $user = User::where('id', $colaborador['user']['id'])->first();
            $user->token = "$token";
            $user->save();

            return response()->json(
                [
                    'token' => $token,
                    'user' => $colaborador['user'],
                    'user_colaboradores' => $colaborador['user_colaboradores'],
                ]
            );
        } else {
            return response()->json(['status' => 'fail'], 401);
        }
    }

    public function buscaColaborador($email, $codigo)
    {
        $colaborador = Users::selectRaw(
            'users.*,
            empresas.name AS nome_empresa,
            users_colaboradores.dispositivos_autorizados_app,
            users_colaboradores.permite_marcacao_ponto_app,
            users_colaboradores.consultas_ponto_vizualizar_ultimas_horas_qtd_dias,
            users_colaboradores.consultas_ponto_vizualizar_ultimas_horas,
            users_colaboradores.consultas_ponto_dias,
            users_colaboradores.consultas_ponto_select_visualizar_editar,
            users_colaboradores.consultas_ponto,
            users_colaboradores.acesso_ao_sistema,
            users_colaboradores.permite_ponto_aplicativo
            '
        )
        ->join('empresas', 'empresas.id', 'users.empresas_id')
        #->join('empresas_enderecos', 'empresas_enderecos.empresas_id', 'empresas.id')
        ->join('users_colaboradores', 'users_colaboradores.users_id', 'users.id')
        ->where('users.email', $email)
        ->where('empresas.codigo', $codigo)
        #->where('users_colaboradores.acesso_ao_sistema', 1)
        #->where('users_colaboradores.permite_ponto_aplicativo', 1)
        ->where('users_colaboradores.status', 1)
        ->where('users.status', 1)
        ->where('empresas.status', 1)
        ->first();

        if (!$colaborador)
            return false;

        $colaboradorArray['user']['id'] = $colaborador->id;
        $colaboradorArray['user']['password'] = $colaborador->password;
        $colaboradorArray['user']['empresas_id'] = $colaborador->empresas_id;
        $colaboradorArray['user']['name'] = $colaborador->name;
        $colaboradorArray['user']['email'] = $colaborador->email;
        $colaboradorArray['user']['matricula'] = $colaborador->matricula;
        $colaboradorArray['user']['idtipo_usuario'] = $colaborador->idtipo_usuario;
        $colaboradorArray['user']['type'] = $colaborador->type;
        $colaboradorArray['user']['token'] = $colaborador->token;
        $colaboradorArray['user']['rg'] = $colaborador->rg;
        $colaboradorArray['user']['cpf'] = $colaborador->cpf;
        $colaboradorArray['user']['photo'] = $colaborador->photo;
        $colaboradorArray['user']['email_verified_at'] = $colaborador->email_verified_at;
        $colaboradorArray['user']['remember_token'] = $colaborador->remember_token;
        $colaboradorArray['user']['status'] = $colaborador->status;
        $colaboradorArray['user']['statusdois'] = $colaborador->statusdois;
        $colaboradorArray['user']['created_at'] = $colaborador->created_at;
        $colaboradorArray['user']['updated_at'] = $colaborador->updated_at;
        $colaboradorArray['user']['deleted_at'] = $colaborador->deleted_at;
        $colaboradorArray['user']['nome_empresa'] = $colaborador->nome_empresa;
        
        $colaboradorArray['user_colaboradores']['dispositivos_autorizados_app'] = $colaborador->dispositivos_autorizados_app;
        $colaboradorArray['user_colaboradores']['permite_marcacao_ponto_app'] = $colaborador->permite_marcacao_ponto_app;
        $colaboradorArray['user_colaboradores']['consultas_ponto_vizualizar_ultimas_horas_qtd_dias'] = $colaborador->consultas_ponto_vizualizar_ultimas_horas_qtd_dias;
        $colaboradorArray['user_colaboradores']['consultas_ponto_vizualizar_ultimas_horas'] = $colaborador->consultas_ponto_vizualizar_ultimas_horas;
        $colaboradorArray['user_colaboradores']['consultas_ponto_dias'] = $colaborador->consultas_ponto_dias;
        $colaboradorArray['user_colaboradores']['consultas_ponto_select_visualizar_editar'] = $colaborador->consultas_ponto_select_visualizar_editar;
        $colaboradorArray['user_colaboradores']['consultas_ponto'] = $colaborador->consultas_ponto;
        $colaboradorArray['user_colaboradores']['acesso_ao_sistema'] = $colaborador->acesso_ao_sistema;
        $colaboradorArray['user_colaboradores']['permite_ponto_aplicativo'] = $colaborador->permite_ponto_aplicativo;
        return $colaboradorArray;
    }
}