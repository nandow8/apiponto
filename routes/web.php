<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('login/', 'authenticate\UsersController@authenticate');

$router->get('registro_marcacao_ponto/{id}', 'Colaborador\RegistroMarcacaoPontosController@show');
$router->post('subirImagemColaborador', 'Colaborador\RegistroMarcacaoPontosController@subirImagemColaborador');
$router->post('registro_marcacao_ponto', 'Colaborador\RegistroMarcacaoPontosController@store');

$router->post('verificasepodebaterponto', 'Colaborador\RegistroMarcacaoPontosController@primeiraVerificaSePodeBaterPonto');
$router->post('updatebaterponto', 'Colaborador\RegistroMarcacaoPontosController@updateBaterPonto');

$router->get('verificapermissoes/{id}', 'Colaborador\UsersColaboradoresController@verificaPermissoes');


$router->post('verificadistanciabaterponto', 'Colaborador\UsersCercasvirtuaisController@verificaDistanciaBaterPonto');